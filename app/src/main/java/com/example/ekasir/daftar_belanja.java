package com.example.ekasir;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;


import java.util.ArrayList;

public class daftar_belanja extends AppCompatActivity {
   private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public ArrayList nama = new ArrayList();
    ArrayList mDataset=new ArrayList();
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_belanja);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        i = getIntent();
        nama = i.getStringArrayListExtra("NAMA");
        mDataset = i.getStringArrayListExtra("HARGA");
        mAdapter = new MyAdapter(nama, mDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
    public void scan(View view) {
        i = new Intent(getApplicationContext(),Barcode.class);
        i.putExtra("NAMA", nama);
        i.putExtra("HARGA",mDataset);
        i.putExtra("CEK",true);
        startActivity(i);
    }
}

