package com.example.ekasir;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.zxing.Result;

import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class Barcode extends AppCompatActivity implements ZXingScannerView.ResultHandler{
    private ZXingScannerView mScannerView;
    public ArrayList n = new ArrayList();
    public ArrayList h = new ArrayList();;
    public Intent i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Log.v("TAG", rawResult.getText()); // Prints scan results
        Log.v("TAG", rawResult.getBarcodeFormat().toString());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        i = getIntent();
        builder.setTitle("Scan Result");
        n.add(rawResult.getText());
        h.add("50000");
        pindah();
       // mScannerView.resumeCameraPreview(this);
    }
    public void pindah(){
        Intent i;
        i = new Intent(getApplicationContext(),daftar_belanja.class);
        i.putExtra("NAMA",n);
        i.putExtra("HARGA",h);
        startActivity(i);
    }

}