package com.example.ekasir;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ekasir.Barcode;
import com.example.ekasir.R;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    ArrayList mDataset;
    ArrayList nama;
    private RelativeLayout ItemList;
    private Context context;
    Intent intent;
    // Provide a suitable constructor (depends on the kind of dataset)
    MyAdapter(ArrayList n,ArrayList myDataset) {
        mDataset = myDataset;
        nama = n;
    }
    MyAdapter(ArrayList myDataset) {
        mDataset = myDataset;
    }
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView nama;
        public TextView harga;
        public TextView banyak;
        public TextView hargaT;
        public Button simpan;
        public Button hapus;

        public ViewHolder(View v) {
            super(v);
            //Untuk Menghubungkan dan Mendapakan Context dari MainActivity
            context = v.getContext();
            nama = (TextView) v.findViewById(R.id.firstLine);
            harga = v.findViewById(R.id.secondLine);
            banyak = v.findViewById(R.id.thirdLine);
            hargaT = v.findViewById(R.id.fourthLine);
            simpan= v.findViewById(R.id.simpan);
            hapus = v.findViewById(R.id.hapus);

            /*ItemList = itemView.findViewById(R.id.item_list);
            ItemList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent;
                    intent = new Intent(context, Barcode.class);
                    context.startActivity(intent);
                }
            });*/
        }
    }

    public void add(int position, String item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }


    // Create new views (invoked by the layout manager)
    //Context context;
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new vie
        //context=parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //final String name = mDataset.get(position);
        holder.nama.setText(""+nama.get(position));
        holder.harga.setText("harga : " + mDataset.get(position));
        //holder.banyak.setText("banyak : " + mDataset.get(position));
        holder.hargaT.setText("harga Total : " + mDataset.get(position));
        /*holder.harga.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
            }
            //@Override
            //public void onClick(View v) {
                //  remove(name);
              //  Intent i = new Intent();
                //i = new Intent(context,Barcode.class);
                /*Toast toast= Toast.makeText(context,
                        "Recycler "+position, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
           //     toast.show();            *///}
        //});
        holder.simpan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(context, Barcode.class);

                context.startActivity(intent);
            }
        });
        holder.hapus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                remove(mDataset.get(position).toString());
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}

